# Allego
I create an webapp with vanilla JS, HTML and CSS and create the following components

- Dropdown
- Diaglo

## Dropdown
You have access to the available methods from the variable **UIdropdownController**

### Available methods. 
#### Initialize
You must specify the component who going to have the ability to open the dropdown content and specify the dropdown content with the data attribute **"data-content"** and the value should be the **ID** from the content element.

```sh
UIdropdownController.init(element)
```
The element initialized have the ability to open and close the dropdown.

**Example**
```html
<div class="dropdown">
    <button class="dropbtn" data-content="ddContent">
        <span class="material-icons" id="modal-open">more_vert</span>
    </button>
    <div id="ddContent" class="dropdown-content">
      <a href="#!" id="dialog">Manage collections and tasks</a>
    </div>
</div>
```

```html
<script>
    UIdropdownController.init('.dropbtn')
</script>
```

#### Open dropdown
You can manually open the drop down with the following method.

```html
<script>
    UIdropdownController.open()
</script>
```
#### Close dropwdown
You can manually close the drop down with the following method.

```html
<script>
    UIdropdownController.close()
</script>
```

## Dialog
You have access to the available methods from the variable **UIdialogController**

### Available methods. 

#### initialize
You must specify the component who going to have the ability to open the dialog and specify the time to wait for the response in milliseconds, the default value is 3000 milliseconds

```sh
UIdialogController.init(element, timeout)
```

**Example**
```html
<a href="#!" id="dialog">Manage collections and tasks</a>
```

```html
<script>
    UIdialogController.init('#dialog', 2000)
</script>
```
#### create
You can manually create the dialog with the following method.
```html
<script>
    UIdialogController.create();
</script>
```
#### open
You can manually open the dialog with the following method.
```html
<script>
    UIdialogController.open()
</script>
```
#### close
You can manually close the dialog with the following method.
```html
<script>
    UIdialogController.close()
</script>
```