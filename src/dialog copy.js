export default class Dialog {

    constructor() {}

    open() {
        const dialog = document.querySelector('body .dialog');
        
        if (!dialog) {
            this.dialog = createDialog();
        } else {
            dialog.classList.remove('d-none');
            setTimeout(() => dialog.classList.add('show') , 100)
        }   
    }
    
    createDialog() {
        const dialog = document.createElement('div');
        dialog.classList.add('dialog');
        dialog.setAttribute('aria-hidden', 'true');
        dialog.appendChild(createOverlay());
    
        document.querySelector('body').appendChild(dialog);
        setTimeout(() => dialog.classList.add('show') , 100)
    
        return dialog;
    }
    
    createOverlay() {
        const el = document.createElement('div');
        el.classList.add('dialog-centered');
        el.appendChild(createContent());
        
        return el;
    }

    createContent() {
        const content = document.createElement('div');
        content.classList.add('dialog-content');
    
        const closeBtn = document.createElement('span') 
        closeBtn.classList.add('close-dialog');
        closeBtn.innerText = 'x';
        closeBtn.addEventListener('click', onClose, false);
    
        const title = document.createElement('h4');
        title.innerText = 'Manage collections and tasks';
        
        const btn = document.createElement('button');
        btn.innerText ='Process';
        btn.addEventListener('click', runAsyncFn, false);
    
        const loader = document.createElement('div');
        loader.classList.add('lds-dual-ring', 'd-none');
    
        const result = document.createElement('div');
        result.setAttribute('id', 'result');
    
        content.appendChild(closeBtn);
        content.appendChild(title);
        content.appendChild(loader);
        content.appendChild(result);
        content.appendChild(btn);
    
        return content;
    }
    
    onClose() {
        const dialog = document.querySelector('.dialog');
        const resultEl = document.querySelector('#result');
        const btnEl = dialog.querySelector('button');
        
        dialog.classList.add('hide');
        
        setTimeout(() => {
            dialog.classList.remove('show');
            dialog.classList.add('d-none');
            btnEl.classList.remove('d-none');
            resultEl.innerHTML = '';
        }, 400)
    }
    
    runAsyncFn() {
        const dialog = document.querySelector('.dialog');
        
        const btnEl = dialog.querySelector('button');
        const loader = dialog.querySelector('.lds-dual-ring');
        const resultEl = dialog.querySelector('#result');
    
        btnEl.classList.add('d-none');
        loader.classList.remove('d-none')
    
        setTimeout(() => {
            loader.classList.add('d-none');
            resultEl.innerHTML = loadContent();
        },3000)
    }
    
    loadContent = function () {
        const calculateTotal = function () {
            const total = Number("`3");
            return total;
        };
        let content = "<div><span>";
        const loaded = 3;
        const total = calculateTotal();
        for (let i = 0; i < loaded; i++) {
            content += "<h2>Test " + i + "</h2>";
        }
        const results = loaded == total ? "<h3>Loaded all items.</h3>" : "<h3>Failed to load all items.</h3>";
        content += results + "<span><div>";
        return content;
    };   
}