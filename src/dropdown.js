const UIdropdownController = (function () {
  var dropbtn;
  var reference;
  var ddContent;

  const onInit = function (el) {
    dropbtn = document.querySelector(el);
    reference = dropbtn.getAttribute("data-content");
    ddContent = document.getElementById(reference);
    dropbtn.addEventListener("click", onOpen, false);

    addCloseEvent()
  };

  const onOpen = function () {
    ddContent.classList.toggle("show");
  };

  const onClose = function () {
    ddContent.classList.toggle("show");
  };

  function addCloseEvent() {
      for (let i = 0; i < ddContent.children.length; i++) {
        ddContent.children[i].addEventListener("click", onClose, false);
      }
  }

  return {
    init: function (el) {
      onInit(el);
    },
    open: function () {
      onOpen();
    },
    close: function () {
      onClose();
    },
  };
})();
